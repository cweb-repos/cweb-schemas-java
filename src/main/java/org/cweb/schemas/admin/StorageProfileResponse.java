/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.admin;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class StorageProfileResponse implements org.apache.thrift.TBase<StorageProfileResponse, StorageProfileResponse._Fields>, java.io.Serializable, Cloneable, Comparable<StorageProfileResponse> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("StorageProfileResponse");

  private static final org.apache.thrift.protocol.TField PRIVATE_STORAGE_PROFILE_FIELD_DESC = new org.apache.thrift.protocol.TField("privateStorageProfile", org.apache.thrift.protocol.TType.STRUCT, (short)1);
  private static final org.apache.thrift.protocol.TField ADMIN_FIELD_DESC = new org.apache.thrift.protocol.TField("admin", org.apache.thrift.protocol.TType.STRUCT, (short)2);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new StorageProfileResponseStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new StorageProfileResponseTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable org.cweb.schemas.storage.PrivateStorageProfile privateStorageProfile; // required
  public @org.apache.thrift.annotation.Nullable org.cweb.schemas.identity.IdentityReference admin; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    PRIVATE_STORAGE_PROFILE((short)1, "privateStorageProfile"),
    ADMIN((short)2, "admin");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // PRIVATE_STORAGE_PROFILE
          return PRIVATE_STORAGE_PROFILE;
        case 2: // ADMIN
          return ADMIN;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.PRIVATE_STORAGE_PROFILE, new org.apache.thrift.meta_data.FieldMetaData("privateStorageProfile", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, org.cweb.schemas.storage.PrivateStorageProfile.class)));
    tmpMap.put(_Fields.ADMIN, new org.apache.thrift.meta_data.FieldMetaData("admin", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, org.cweb.schemas.identity.IdentityReference.class)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(StorageProfileResponse.class, metaDataMap);
  }

  public StorageProfileResponse() {
  }

  public StorageProfileResponse(
    org.cweb.schemas.storage.PrivateStorageProfile privateStorageProfile,
    org.cweb.schemas.identity.IdentityReference admin)
  {
    this();
    this.privateStorageProfile = privateStorageProfile;
    this.admin = admin;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public StorageProfileResponse(StorageProfileResponse other) {
    if (other.isSetPrivateStorageProfile()) {
      this.privateStorageProfile = new org.cweb.schemas.storage.PrivateStorageProfile(other.privateStorageProfile);
    }
    if (other.isSetAdmin()) {
      this.admin = new org.cweb.schemas.identity.IdentityReference(other.admin);
    }
  }

  @Override
  public StorageProfileResponse deepCopy() {
    return new StorageProfileResponse(this);
  }

  @Override
  public void clear() {
    this.privateStorageProfile = null;
    this.admin = null;
  }

  @org.apache.thrift.annotation.Nullable
  public org.cweb.schemas.storage.PrivateStorageProfile getPrivateStorageProfile() {
    return this.privateStorageProfile;
  }

  public StorageProfileResponse setPrivateStorageProfile(@org.apache.thrift.annotation.Nullable org.cweb.schemas.storage.PrivateStorageProfile privateStorageProfile) {
    this.privateStorageProfile = privateStorageProfile;
    return this;
  }

  public void unsetPrivateStorageProfile() {
    this.privateStorageProfile = null;
  }

  /** Returns true if field privateStorageProfile is set (has been assigned a value) and false otherwise */
  public boolean isSetPrivateStorageProfile() {
    return this.privateStorageProfile != null;
  }

  public void setPrivateStorageProfileIsSet(boolean value) {
    if (!value) {
      this.privateStorageProfile = null;
    }
  }

  @org.apache.thrift.annotation.Nullable
  public org.cweb.schemas.identity.IdentityReference getAdmin() {
    return this.admin;
  }

  public StorageProfileResponse setAdmin(@org.apache.thrift.annotation.Nullable org.cweb.schemas.identity.IdentityReference admin) {
    this.admin = admin;
    return this;
  }

  public void unsetAdmin() {
    this.admin = null;
  }

  /** Returns true if field admin is set (has been assigned a value) and false otherwise */
  public boolean isSetAdmin() {
    return this.admin != null;
  }

  public void setAdminIsSet(boolean value) {
    if (!value) {
      this.admin = null;
    }
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case PRIVATE_STORAGE_PROFILE:
      if (value == null) {
        unsetPrivateStorageProfile();
      } else {
        setPrivateStorageProfile((org.cweb.schemas.storage.PrivateStorageProfile)value);
      }
      break;

    case ADMIN:
      if (value == null) {
        unsetAdmin();
      } else {
        setAdmin((org.cweb.schemas.identity.IdentityReference)value);
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case PRIVATE_STORAGE_PROFILE:
      return getPrivateStorageProfile();

    case ADMIN:
      return getAdmin();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case PRIVATE_STORAGE_PROFILE:
      return isSetPrivateStorageProfile();
    case ADMIN:
      return isSetAdmin();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof StorageProfileResponse)
      return this.equals((StorageProfileResponse)that);
    return false;
  }

  public boolean equals(StorageProfileResponse that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_privateStorageProfile = true && this.isSetPrivateStorageProfile();
    boolean that_present_privateStorageProfile = true && that.isSetPrivateStorageProfile();
    if (this_present_privateStorageProfile || that_present_privateStorageProfile) {
      if (!(this_present_privateStorageProfile && that_present_privateStorageProfile))
        return false;
      if (!this.privateStorageProfile.equals(that.privateStorageProfile))
        return false;
    }

    boolean this_present_admin = true && this.isSetAdmin();
    boolean that_present_admin = true && that.isSetAdmin();
    if (this_present_admin || that_present_admin) {
      if (!(this_present_admin && that_present_admin))
        return false;
      if (!this.admin.equals(that.admin))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetPrivateStorageProfile()) ? 131071 : 524287);
    if (isSetPrivateStorageProfile())
      hashCode = hashCode * 8191 + privateStorageProfile.hashCode();

    hashCode = hashCode * 8191 + ((isSetAdmin()) ? 131071 : 524287);
    if (isSetAdmin())
      hashCode = hashCode * 8191 + admin.hashCode();

    return hashCode;
  }

  @Override
  public int compareTo(StorageProfileResponse other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetPrivateStorageProfile(), other.isSetPrivateStorageProfile());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPrivateStorageProfile()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.privateStorageProfile, other.privateStorageProfile);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetAdmin(), other.isSetAdmin());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAdmin()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.admin, other.admin);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("StorageProfileResponse(");
    boolean first = true;

    sb.append("privateStorageProfile:");
    if (this.privateStorageProfile == null) {
      sb.append("null");
    } else {
      sb.append(this.privateStorageProfile);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("admin:");
    if (this.admin == null) {
      sb.append("null");
    } else {
      sb.append(this.admin);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (privateStorageProfile == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'privateStorageProfile' was not present! Struct: " + toString());
    }
    if (admin == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'admin' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
    if (privateStorageProfile != null) {
      privateStorageProfile.validate();
    }
    if (admin != null) {
      admin.validate();
    }
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class StorageProfileResponseStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public StorageProfileResponseStandardScheme getScheme() {
      return new StorageProfileResponseStandardScheme();
    }
  }

  private static class StorageProfileResponseStandardScheme extends org.apache.thrift.scheme.StandardScheme<StorageProfileResponse> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, StorageProfileResponse struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // PRIVATE_STORAGE_PROFILE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.privateStorageProfile = new org.cweb.schemas.storage.PrivateStorageProfile();
              struct.privateStorageProfile.read(iprot);
              struct.setPrivateStorageProfileIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // ADMIN
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.admin = new org.cweb.schemas.identity.IdentityReference();
              struct.admin.read(iprot);
              struct.setAdminIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, StorageProfileResponse struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.privateStorageProfile != null) {
        oprot.writeFieldBegin(PRIVATE_STORAGE_PROFILE_FIELD_DESC);
        struct.privateStorageProfile.write(oprot);
        oprot.writeFieldEnd();
      }
      if (struct.admin != null) {
        oprot.writeFieldBegin(ADMIN_FIELD_DESC);
        struct.admin.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class StorageProfileResponseTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public StorageProfileResponseTupleScheme getScheme() {
      return new StorageProfileResponseTupleScheme();
    }
  }

  private static class StorageProfileResponseTupleScheme extends org.apache.thrift.scheme.TupleScheme<StorageProfileResponse> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, StorageProfileResponse struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.privateStorageProfile.write(oprot);
      struct.admin.write(oprot);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, StorageProfileResponse struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.privateStorageProfile = new org.cweb.schemas.storage.PrivateStorageProfile();
      struct.privateStorageProfile.read(iprot);
      struct.setPrivateStorageProfileIsSet(true);
      struct.admin = new org.cweb.schemas.identity.IdentityReference();
      struct.admin.read(iprot);
      struct.setAdminIsSet(true);
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

