/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.comm.object;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class SharedObject implements org.apache.thrift.TBase<SharedObject, SharedObject._Fields>, java.io.Serializable, Cloneable, Comparable<SharedObject> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("SharedObject");

  private static final org.apache.thrift.protocol.TField FROM_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("fromId", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField OBJECT_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("objectId", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField TYPE_FIELD_DESC = new org.apache.thrift.protocol.TField("type", org.apache.thrift.protocol.TType.I32, (short)3);
  private static final org.apache.thrift.protocol.TField POLL_INTERVAL_FIELD_DESC = new org.apache.thrift.protocol.TField("pollInterval", org.apache.thrift.protocol.TType.I64, (short)4);
  private static final org.apache.thrift.protocol.TField CREATED_AT_FIELD_DESC = new org.apache.thrift.protocol.TField("createdAt", org.apache.thrift.protocol.TType.I64, (short)5);
  private static final org.apache.thrift.protocol.TField PAYLOAD_FIELD_DESC = new org.apache.thrift.protocol.TField("payload", org.apache.thrift.protocol.TType.STRUCT, (short)20);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new SharedObjectStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new SharedObjectTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer fromId; // required
  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer objectId; // required
  /**
   * 
   * @see SharedObjectDeliveryType
   */
  public @org.apache.thrift.annotation.Nullable SharedObjectDeliveryType type; // required
  public long pollInterval; // required
  public long createdAt; // required
  public @org.apache.thrift.annotation.Nullable org.cweb.schemas.wire.TypedPayload payload; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    FROM_ID((short)1, "fromId"),
    OBJECT_ID((short)2, "objectId"),
    /**
     * 
     * @see SharedObjectDeliveryType
     */
    TYPE((short)3, "type"),
    POLL_INTERVAL((short)4, "pollInterval"),
    CREATED_AT((short)5, "createdAt"),
    PAYLOAD((short)20, "payload");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // FROM_ID
          return FROM_ID;
        case 2: // OBJECT_ID
          return OBJECT_ID;
        case 3: // TYPE
          return TYPE;
        case 4: // POLL_INTERVAL
          return POLL_INTERVAL;
        case 5: // CREATED_AT
          return CREATED_AT;
        case 20: // PAYLOAD
          return PAYLOAD;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __POLLINTERVAL_ISSET_ID = 0;
  private static final int __CREATEDAT_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.FROM_ID, new org.apache.thrift.meta_data.FieldMetaData("fromId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    tmpMap.put(_Fields.OBJECT_ID, new org.apache.thrift.meta_data.FieldMetaData("objectId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    tmpMap.put(_Fields.TYPE, new org.apache.thrift.meta_data.FieldMetaData("type", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.EnumMetaData(org.apache.thrift.protocol.TType.ENUM, SharedObjectDeliveryType.class)));
    tmpMap.put(_Fields.POLL_INTERVAL, new org.apache.thrift.meta_data.FieldMetaData("pollInterval", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.CREATED_AT, new org.apache.thrift.meta_data.FieldMetaData("createdAt", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.PAYLOAD, new org.apache.thrift.meta_data.FieldMetaData("payload", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, org.cweb.schemas.wire.TypedPayload.class)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(SharedObject.class, metaDataMap);
  }

  public SharedObject() {
  }

  public SharedObject(
    java.nio.ByteBuffer fromId,
    java.nio.ByteBuffer objectId,
    SharedObjectDeliveryType type,
    long pollInterval,
    long createdAt,
    org.cweb.schemas.wire.TypedPayload payload)
  {
    this();
    this.fromId = org.apache.thrift.TBaseHelper.copyBinary(fromId);
    this.objectId = org.apache.thrift.TBaseHelper.copyBinary(objectId);
    this.type = type;
    this.pollInterval = pollInterval;
    setPollIntervalIsSet(true);
    this.createdAt = createdAt;
    setCreatedAtIsSet(true);
    this.payload = payload;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public SharedObject(SharedObject other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetFromId()) {
      this.fromId = org.apache.thrift.TBaseHelper.copyBinary(other.fromId);
    }
    if (other.isSetObjectId()) {
      this.objectId = org.apache.thrift.TBaseHelper.copyBinary(other.objectId);
    }
    if (other.isSetType()) {
      this.type = other.type;
    }
    this.pollInterval = other.pollInterval;
    this.createdAt = other.createdAt;
    if (other.isSetPayload()) {
      this.payload = new org.cweb.schemas.wire.TypedPayload(other.payload);
    }
  }

  @Override
  public SharedObject deepCopy() {
    return new SharedObject(this);
  }

  @Override
  public void clear() {
    this.fromId = null;
    this.objectId = null;
    this.type = null;
    setPollIntervalIsSet(false);
    this.pollInterval = 0;
    setCreatedAtIsSet(false);
    this.createdAt = 0;
    this.payload = null;
  }

  public byte[] getFromId() {
    setFromId(org.apache.thrift.TBaseHelper.rightSize(fromId));
    return fromId == null ? null : fromId.array();
  }

  public java.nio.ByteBuffer bufferForFromId() {
    return org.apache.thrift.TBaseHelper.copyBinary(fromId);
  }

  public SharedObject setFromId(byte[] fromId) {
    this.fromId = fromId == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(fromId.clone());
    return this;
  }

  public SharedObject setFromId(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer fromId) {
    this.fromId = org.apache.thrift.TBaseHelper.copyBinary(fromId);
    return this;
  }

  public void unsetFromId() {
    this.fromId = null;
  }

  /** Returns true if field fromId is set (has been assigned a value) and false otherwise */
  public boolean isSetFromId() {
    return this.fromId != null;
  }

  public void setFromIdIsSet(boolean value) {
    if (!value) {
      this.fromId = null;
    }
  }

  public byte[] getObjectId() {
    setObjectId(org.apache.thrift.TBaseHelper.rightSize(objectId));
    return objectId == null ? null : objectId.array();
  }

  public java.nio.ByteBuffer bufferForObjectId() {
    return org.apache.thrift.TBaseHelper.copyBinary(objectId);
  }

  public SharedObject setObjectId(byte[] objectId) {
    this.objectId = objectId == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(objectId.clone());
    return this;
  }

  public SharedObject setObjectId(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer objectId) {
    this.objectId = org.apache.thrift.TBaseHelper.copyBinary(objectId);
    return this;
  }

  public void unsetObjectId() {
    this.objectId = null;
  }

  /** Returns true if field objectId is set (has been assigned a value) and false otherwise */
  public boolean isSetObjectId() {
    return this.objectId != null;
  }

  public void setObjectIdIsSet(boolean value) {
    if (!value) {
      this.objectId = null;
    }
  }

  /**
   * 
   * @see SharedObjectDeliveryType
   */
  @org.apache.thrift.annotation.Nullable
  public SharedObjectDeliveryType getType() {
    return this.type;
  }

  /**
   * 
   * @see SharedObjectDeliveryType
   */
  public SharedObject setType(@org.apache.thrift.annotation.Nullable SharedObjectDeliveryType type) {
    this.type = type;
    return this;
  }

  public void unsetType() {
    this.type = null;
  }

  /** Returns true if field type is set (has been assigned a value) and false otherwise */
  public boolean isSetType() {
    return this.type != null;
  }

  public void setTypeIsSet(boolean value) {
    if (!value) {
      this.type = null;
    }
  }

  public long getPollInterval() {
    return this.pollInterval;
  }

  public SharedObject setPollInterval(long pollInterval) {
    this.pollInterval = pollInterval;
    setPollIntervalIsSet(true);
    return this;
  }

  public void unsetPollInterval() {
    __isset_bitfield = org.apache.thrift.EncodingUtils.clearBit(__isset_bitfield, __POLLINTERVAL_ISSET_ID);
  }

  /** Returns true if field pollInterval is set (has been assigned a value) and false otherwise */
  public boolean isSetPollInterval() {
    return org.apache.thrift.EncodingUtils.testBit(__isset_bitfield, __POLLINTERVAL_ISSET_ID);
  }

  public void setPollIntervalIsSet(boolean value) {
    __isset_bitfield = org.apache.thrift.EncodingUtils.setBit(__isset_bitfield, __POLLINTERVAL_ISSET_ID, value);
  }

  public long getCreatedAt() {
    return this.createdAt;
  }

  public SharedObject setCreatedAt(long createdAt) {
    this.createdAt = createdAt;
    setCreatedAtIsSet(true);
    return this;
  }

  public void unsetCreatedAt() {
    __isset_bitfield = org.apache.thrift.EncodingUtils.clearBit(__isset_bitfield, __CREATEDAT_ISSET_ID);
  }

  /** Returns true if field createdAt is set (has been assigned a value) and false otherwise */
  public boolean isSetCreatedAt() {
    return org.apache.thrift.EncodingUtils.testBit(__isset_bitfield, __CREATEDAT_ISSET_ID);
  }

  public void setCreatedAtIsSet(boolean value) {
    __isset_bitfield = org.apache.thrift.EncodingUtils.setBit(__isset_bitfield, __CREATEDAT_ISSET_ID, value);
  }

  @org.apache.thrift.annotation.Nullable
  public org.cweb.schemas.wire.TypedPayload getPayload() {
    return this.payload;
  }

  public SharedObject setPayload(@org.apache.thrift.annotation.Nullable org.cweb.schemas.wire.TypedPayload payload) {
    this.payload = payload;
    return this;
  }

  public void unsetPayload() {
    this.payload = null;
  }

  /** Returns true if field payload is set (has been assigned a value) and false otherwise */
  public boolean isSetPayload() {
    return this.payload != null;
  }

  public void setPayloadIsSet(boolean value) {
    if (!value) {
      this.payload = null;
    }
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case FROM_ID:
      if (value == null) {
        unsetFromId();
      } else {
        if (value instanceof byte[]) {
          setFromId((byte[])value);
        } else {
          setFromId((java.nio.ByteBuffer)value);
        }
      }
      break;

    case OBJECT_ID:
      if (value == null) {
        unsetObjectId();
      } else {
        if (value instanceof byte[]) {
          setObjectId((byte[])value);
        } else {
          setObjectId((java.nio.ByteBuffer)value);
        }
      }
      break;

    case TYPE:
      if (value == null) {
        unsetType();
      } else {
        setType((SharedObjectDeliveryType)value);
      }
      break;

    case POLL_INTERVAL:
      if (value == null) {
        unsetPollInterval();
      } else {
        setPollInterval((java.lang.Long)value);
      }
      break;

    case CREATED_AT:
      if (value == null) {
        unsetCreatedAt();
      } else {
        setCreatedAt((java.lang.Long)value);
      }
      break;

    case PAYLOAD:
      if (value == null) {
        unsetPayload();
      } else {
        setPayload((org.cweb.schemas.wire.TypedPayload)value);
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case FROM_ID:
      return getFromId();

    case OBJECT_ID:
      return getObjectId();

    case TYPE:
      return getType();

    case POLL_INTERVAL:
      return getPollInterval();

    case CREATED_AT:
      return getCreatedAt();

    case PAYLOAD:
      return getPayload();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case FROM_ID:
      return isSetFromId();
    case OBJECT_ID:
      return isSetObjectId();
    case TYPE:
      return isSetType();
    case POLL_INTERVAL:
      return isSetPollInterval();
    case CREATED_AT:
      return isSetCreatedAt();
    case PAYLOAD:
      return isSetPayload();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof SharedObject)
      return this.equals((SharedObject)that);
    return false;
  }

  public boolean equals(SharedObject that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_fromId = true && this.isSetFromId();
    boolean that_present_fromId = true && that.isSetFromId();
    if (this_present_fromId || that_present_fromId) {
      if (!(this_present_fromId && that_present_fromId))
        return false;
      if (!this.fromId.equals(that.fromId))
        return false;
    }

    boolean this_present_objectId = true && this.isSetObjectId();
    boolean that_present_objectId = true && that.isSetObjectId();
    if (this_present_objectId || that_present_objectId) {
      if (!(this_present_objectId && that_present_objectId))
        return false;
      if (!this.objectId.equals(that.objectId))
        return false;
    }

    boolean this_present_type = true && this.isSetType();
    boolean that_present_type = true && that.isSetType();
    if (this_present_type || that_present_type) {
      if (!(this_present_type && that_present_type))
        return false;
      if (!this.type.equals(that.type))
        return false;
    }

    boolean this_present_pollInterval = true;
    boolean that_present_pollInterval = true;
    if (this_present_pollInterval || that_present_pollInterval) {
      if (!(this_present_pollInterval && that_present_pollInterval))
        return false;
      if (this.pollInterval != that.pollInterval)
        return false;
    }

    boolean this_present_createdAt = true;
    boolean that_present_createdAt = true;
    if (this_present_createdAt || that_present_createdAt) {
      if (!(this_present_createdAt && that_present_createdAt))
        return false;
      if (this.createdAt != that.createdAt)
        return false;
    }

    boolean this_present_payload = true && this.isSetPayload();
    boolean that_present_payload = true && that.isSetPayload();
    if (this_present_payload || that_present_payload) {
      if (!(this_present_payload && that_present_payload))
        return false;
      if (!this.payload.equals(that.payload))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetFromId()) ? 131071 : 524287);
    if (isSetFromId())
      hashCode = hashCode * 8191 + fromId.hashCode();

    hashCode = hashCode * 8191 + ((isSetObjectId()) ? 131071 : 524287);
    if (isSetObjectId())
      hashCode = hashCode * 8191 + objectId.hashCode();

    hashCode = hashCode * 8191 + ((isSetType()) ? 131071 : 524287);
    if (isSetType())
      hashCode = hashCode * 8191 + type.getValue();

    hashCode = hashCode * 8191 + org.apache.thrift.TBaseHelper.hashCode(pollInterval);

    hashCode = hashCode * 8191 + org.apache.thrift.TBaseHelper.hashCode(createdAt);

    hashCode = hashCode * 8191 + ((isSetPayload()) ? 131071 : 524287);
    if (isSetPayload())
      hashCode = hashCode * 8191 + payload.hashCode();

    return hashCode;
  }

  @Override
  public int compareTo(SharedObject other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetFromId(), other.isSetFromId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetFromId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.fromId, other.fromId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetObjectId(), other.isSetObjectId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetObjectId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.objectId, other.objectId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetType(), other.isSetType());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetType()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.type, other.type);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetPollInterval(), other.isSetPollInterval());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPollInterval()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.pollInterval, other.pollInterval);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetCreatedAt(), other.isSetCreatedAt());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCreatedAt()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.createdAt, other.createdAt);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetPayload(), other.isSetPayload());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPayload()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.payload, other.payload);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("SharedObject(");
    boolean first = true;

    sb.append("fromId:");
    if (this.fromId == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.fromId, sb);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("objectId:");
    if (this.objectId == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.objectId, sb);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("type:");
    if (this.type == null) {
      sb.append("null");
    } else {
      sb.append(this.type);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("pollInterval:");
    sb.append(this.pollInterval);
    first = false;
    if (!first) sb.append(", ");
    sb.append("createdAt:");
    sb.append(this.createdAt);
    first = false;
    if (!first) sb.append(", ");
    sb.append("payload:");
    if (this.payload == null) {
      sb.append("null");
    } else {
      sb.append(this.payload);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (fromId == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'fromId' was not present! Struct: " + toString());
    }
    if (objectId == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'objectId' was not present! Struct: " + toString());
    }
    if (type == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'type' was not present! Struct: " + toString());
    }
    // alas, we cannot check 'pollInterval' because it's a primitive and you chose the non-beans generator.
    // alas, we cannot check 'createdAt' because it's a primitive and you chose the non-beans generator.
    if (payload == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'payload' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
    if (payload != null) {
      payload.validate();
    }
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class SharedObjectStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public SharedObjectStandardScheme getScheme() {
      return new SharedObjectStandardScheme();
    }
  }

  private static class SharedObjectStandardScheme extends org.apache.thrift.scheme.StandardScheme<SharedObject> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, SharedObject struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // FROM_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.fromId = iprot.readBinary();
              struct.setFromIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // OBJECT_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.objectId = iprot.readBinary();
              struct.setObjectIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // TYPE
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.type = org.cweb.schemas.comm.object.SharedObjectDeliveryType.findByValue(iprot.readI32());
              struct.setTypeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // POLL_INTERVAL
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.pollInterval = iprot.readI64();
              struct.setPollIntervalIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // CREATED_AT
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.createdAt = iprot.readI64();
              struct.setCreatedAtIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 20: // PAYLOAD
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.payload = new org.cweb.schemas.wire.TypedPayload();
              struct.payload.read(iprot);
              struct.setPayloadIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetPollInterval()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'pollInterval' was not found in serialized data! Struct: " + toString());
      }
      if (!struct.isSetCreatedAt()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'createdAt' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, SharedObject struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.fromId != null) {
        oprot.writeFieldBegin(FROM_ID_FIELD_DESC);
        oprot.writeBinary(struct.fromId);
        oprot.writeFieldEnd();
      }
      if (struct.objectId != null) {
        oprot.writeFieldBegin(OBJECT_ID_FIELD_DESC);
        oprot.writeBinary(struct.objectId);
        oprot.writeFieldEnd();
      }
      if (struct.type != null) {
        oprot.writeFieldBegin(TYPE_FIELD_DESC);
        oprot.writeI32(struct.type.getValue());
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(POLL_INTERVAL_FIELD_DESC);
      oprot.writeI64(struct.pollInterval);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(CREATED_AT_FIELD_DESC);
      oprot.writeI64(struct.createdAt);
      oprot.writeFieldEnd();
      if (struct.payload != null) {
        oprot.writeFieldBegin(PAYLOAD_FIELD_DESC);
        struct.payload.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class SharedObjectTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public SharedObjectTupleScheme getScheme() {
      return new SharedObjectTupleScheme();
    }
  }

  private static class SharedObjectTupleScheme extends org.apache.thrift.scheme.TupleScheme<SharedObject> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, SharedObject struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      oprot.writeBinary(struct.fromId);
      oprot.writeBinary(struct.objectId);
      oprot.writeI32(struct.type.getValue());
      oprot.writeI64(struct.pollInterval);
      oprot.writeI64(struct.createdAt);
      struct.payload.write(oprot);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, SharedObject struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.fromId = iprot.readBinary();
      struct.setFromIdIsSet(true);
      struct.objectId = iprot.readBinary();
      struct.setObjectIdIsSet(true);
      struct.type = org.cweb.schemas.comm.object.SharedObjectDeliveryType.findByValue(iprot.readI32());
      struct.setTypeIsSet(true);
      struct.pollInterval = iprot.readI64();
      struct.setPollIntervalIsSet(true);
      struct.createdAt = iprot.readI64();
      struct.setCreatedAtIsSet(true);
      struct.payload = new org.cweb.schemas.wire.TypedPayload();
      struct.payload.read(iprot);
      struct.setPayloadIsSet(true);
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

