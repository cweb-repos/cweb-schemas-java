/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.comm.session;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class CommSessionSeedCryptoEnvelope implements org.apache.thrift.TBase<CommSessionSeedCryptoEnvelope, CommSessionSeedCryptoEnvelope._Fields>, java.io.Serializable, Cloneable, Comparable<CommSessionSeedCryptoEnvelope> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("CommSessionSeedCryptoEnvelope");

  private static final org.apache.thrift.protocol.TField CRYPTO_ENVELOPE_FIELD_DESC = new org.apache.thrift.protocol.TField("cryptoEnvelope", org.apache.thrift.protocol.TType.STRING, (short)1);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new CommSessionSeedCryptoEnvelopeStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new CommSessionSeedCryptoEnvelopeTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer cryptoEnvelope; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    CRYPTO_ENVELOPE((short)1, "cryptoEnvelope");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // CRYPTO_ENVELOPE
          return CRYPTO_ENVELOPE;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.CRYPTO_ENVELOPE, new org.apache.thrift.meta_data.FieldMetaData("cryptoEnvelope", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(CommSessionSeedCryptoEnvelope.class, metaDataMap);
  }

  public CommSessionSeedCryptoEnvelope() {
  }

  public CommSessionSeedCryptoEnvelope(
    java.nio.ByteBuffer cryptoEnvelope)
  {
    this();
    this.cryptoEnvelope = org.apache.thrift.TBaseHelper.copyBinary(cryptoEnvelope);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public CommSessionSeedCryptoEnvelope(CommSessionSeedCryptoEnvelope other) {
    if (other.isSetCryptoEnvelope()) {
      this.cryptoEnvelope = org.apache.thrift.TBaseHelper.copyBinary(other.cryptoEnvelope);
    }
  }

  @Override
  public CommSessionSeedCryptoEnvelope deepCopy() {
    return new CommSessionSeedCryptoEnvelope(this);
  }

  @Override
  public void clear() {
    this.cryptoEnvelope = null;
  }

  public byte[] getCryptoEnvelope() {
    setCryptoEnvelope(org.apache.thrift.TBaseHelper.rightSize(cryptoEnvelope));
    return cryptoEnvelope == null ? null : cryptoEnvelope.array();
  }

  public java.nio.ByteBuffer bufferForCryptoEnvelope() {
    return org.apache.thrift.TBaseHelper.copyBinary(cryptoEnvelope);
  }

  public CommSessionSeedCryptoEnvelope setCryptoEnvelope(byte[] cryptoEnvelope) {
    this.cryptoEnvelope = cryptoEnvelope == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(cryptoEnvelope.clone());
    return this;
  }

  public CommSessionSeedCryptoEnvelope setCryptoEnvelope(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer cryptoEnvelope) {
    this.cryptoEnvelope = org.apache.thrift.TBaseHelper.copyBinary(cryptoEnvelope);
    return this;
  }

  public void unsetCryptoEnvelope() {
    this.cryptoEnvelope = null;
  }

  /** Returns true if field cryptoEnvelope is set (has been assigned a value) and false otherwise */
  public boolean isSetCryptoEnvelope() {
    return this.cryptoEnvelope != null;
  }

  public void setCryptoEnvelopeIsSet(boolean value) {
    if (!value) {
      this.cryptoEnvelope = null;
    }
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case CRYPTO_ENVELOPE:
      if (value == null) {
        unsetCryptoEnvelope();
      } else {
        if (value instanceof byte[]) {
          setCryptoEnvelope((byte[])value);
        } else {
          setCryptoEnvelope((java.nio.ByteBuffer)value);
        }
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case CRYPTO_ENVELOPE:
      return getCryptoEnvelope();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case CRYPTO_ENVELOPE:
      return isSetCryptoEnvelope();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof CommSessionSeedCryptoEnvelope)
      return this.equals((CommSessionSeedCryptoEnvelope)that);
    return false;
  }

  public boolean equals(CommSessionSeedCryptoEnvelope that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_cryptoEnvelope = true && this.isSetCryptoEnvelope();
    boolean that_present_cryptoEnvelope = true && that.isSetCryptoEnvelope();
    if (this_present_cryptoEnvelope || that_present_cryptoEnvelope) {
      if (!(this_present_cryptoEnvelope && that_present_cryptoEnvelope))
        return false;
      if (!this.cryptoEnvelope.equals(that.cryptoEnvelope))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetCryptoEnvelope()) ? 131071 : 524287);
    if (isSetCryptoEnvelope())
      hashCode = hashCode * 8191 + cryptoEnvelope.hashCode();

    return hashCode;
  }

  @Override
  public int compareTo(CommSessionSeedCryptoEnvelope other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetCryptoEnvelope(), other.isSetCryptoEnvelope());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCryptoEnvelope()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.cryptoEnvelope, other.cryptoEnvelope);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("CommSessionSeedCryptoEnvelope(");
    boolean first = true;

    sb.append("cryptoEnvelope:");
    if (this.cryptoEnvelope == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.cryptoEnvelope, sb);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (cryptoEnvelope == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'cryptoEnvelope' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class CommSessionSeedCryptoEnvelopeStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public CommSessionSeedCryptoEnvelopeStandardScheme getScheme() {
      return new CommSessionSeedCryptoEnvelopeStandardScheme();
    }
  }

  private static class CommSessionSeedCryptoEnvelopeStandardScheme extends org.apache.thrift.scheme.StandardScheme<CommSessionSeedCryptoEnvelope> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, CommSessionSeedCryptoEnvelope struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // CRYPTO_ENVELOPE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.cryptoEnvelope = iprot.readBinary();
              struct.setCryptoEnvelopeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, CommSessionSeedCryptoEnvelope struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.cryptoEnvelope != null) {
        oprot.writeFieldBegin(CRYPTO_ENVELOPE_FIELD_DESC);
        oprot.writeBinary(struct.cryptoEnvelope);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class CommSessionSeedCryptoEnvelopeTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public CommSessionSeedCryptoEnvelopeTupleScheme getScheme() {
      return new CommSessionSeedCryptoEnvelopeTupleScheme();
    }
  }

  private static class CommSessionSeedCryptoEnvelopeTupleScheme extends org.apache.thrift.scheme.TupleScheme<CommSessionSeedCryptoEnvelope> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, CommSessionSeedCryptoEnvelope struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      oprot.writeBinary(struct.cryptoEnvelope);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, CommSessionSeedCryptoEnvelope struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.cryptoEnvelope = iprot.readBinary();
      struct.setCryptoEnvelopeIsSet(true);
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

