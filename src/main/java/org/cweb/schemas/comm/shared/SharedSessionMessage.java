/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.comm.shared;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class SharedSessionMessage implements org.apache.thrift.TBase<SharedSessionMessage, SharedSessionMessage._Fields>, java.io.Serializable, Cloneable, Comparable<SharedSessionMessage> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("SharedSessionMessage");

  private static final org.apache.thrift.protocol.TField METADATA_FIELD_DESC = new org.apache.thrift.protocol.TField("metadata", org.apache.thrift.protocol.TType.STRUCT, (short)1);
  private static final org.apache.thrift.protocol.TField PREVIOUS_MESSAGE_METADATA_FIELD_DESC = new org.apache.thrift.protocol.TField("previousMessageMetadata", org.apache.thrift.protocol.TType.LIST, (short)2);
  private static final org.apache.thrift.protocol.TField PAYLOAD_FIELD_DESC = new org.apache.thrift.protocol.TField("payload", org.apache.thrift.protocol.TType.STRUCT, (short)21);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new SharedSessionMessageStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new SharedSessionMessageTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable SharedSessionMessageMetadata metadata; // required
  public @org.apache.thrift.annotation.Nullable java.util.List<SharedSessionMessageMetadata> previousMessageMetadata; // required
  public @org.apache.thrift.annotation.Nullable org.cweb.schemas.wire.TypedPayload payload; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    METADATA((short)1, "metadata"),
    PREVIOUS_MESSAGE_METADATA((short)2, "previousMessageMetadata"),
    PAYLOAD((short)21, "payload");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // METADATA
          return METADATA;
        case 2: // PREVIOUS_MESSAGE_METADATA
          return PREVIOUS_MESSAGE_METADATA;
        case 21: // PAYLOAD
          return PAYLOAD;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.METADATA, new org.apache.thrift.meta_data.FieldMetaData("metadata", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, SharedSessionMessageMetadata.class)));
    tmpMap.put(_Fields.PREVIOUS_MESSAGE_METADATA, new org.apache.thrift.meta_data.FieldMetaData("previousMessageMetadata", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, SharedSessionMessageMetadata.class))));
    tmpMap.put(_Fields.PAYLOAD, new org.apache.thrift.meta_data.FieldMetaData("payload", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, org.cweb.schemas.wire.TypedPayload.class)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(SharedSessionMessage.class, metaDataMap);
  }

  public SharedSessionMessage() {
  }

  public SharedSessionMessage(
    SharedSessionMessageMetadata metadata,
    java.util.List<SharedSessionMessageMetadata> previousMessageMetadata,
    org.cweb.schemas.wire.TypedPayload payload)
  {
    this();
    this.metadata = metadata;
    this.previousMessageMetadata = previousMessageMetadata;
    this.payload = payload;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public SharedSessionMessage(SharedSessionMessage other) {
    if (other.isSetMetadata()) {
      this.metadata = new SharedSessionMessageMetadata(other.metadata);
    }
    if (other.isSetPreviousMessageMetadata()) {
      java.util.List<SharedSessionMessageMetadata> __this__previousMessageMetadata = new java.util.ArrayList<SharedSessionMessageMetadata>(other.previousMessageMetadata.size());
      for (SharedSessionMessageMetadata other_element : other.previousMessageMetadata) {
        __this__previousMessageMetadata.add(new SharedSessionMessageMetadata(other_element));
      }
      this.previousMessageMetadata = __this__previousMessageMetadata;
    }
    if (other.isSetPayload()) {
      this.payload = new org.cweb.schemas.wire.TypedPayload(other.payload);
    }
  }

  @Override
  public SharedSessionMessage deepCopy() {
    return new SharedSessionMessage(this);
  }

  @Override
  public void clear() {
    this.metadata = null;
    this.previousMessageMetadata = null;
    this.payload = null;
  }

  @org.apache.thrift.annotation.Nullable
  public SharedSessionMessageMetadata getMetadata() {
    return this.metadata;
  }

  public SharedSessionMessage setMetadata(@org.apache.thrift.annotation.Nullable SharedSessionMessageMetadata metadata) {
    this.metadata = metadata;
    return this;
  }

  public void unsetMetadata() {
    this.metadata = null;
  }

  /** Returns true if field metadata is set (has been assigned a value) and false otherwise */
  public boolean isSetMetadata() {
    return this.metadata != null;
  }

  public void setMetadataIsSet(boolean value) {
    if (!value) {
      this.metadata = null;
    }
  }

  public int getPreviousMessageMetadataSize() {
    return (this.previousMessageMetadata == null) ? 0 : this.previousMessageMetadata.size();
  }

  @org.apache.thrift.annotation.Nullable
  public java.util.Iterator<SharedSessionMessageMetadata> getPreviousMessageMetadataIterator() {
    return (this.previousMessageMetadata == null) ? null : this.previousMessageMetadata.iterator();
  }

  public void addToPreviousMessageMetadata(SharedSessionMessageMetadata elem) {
    if (this.previousMessageMetadata == null) {
      this.previousMessageMetadata = new java.util.ArrayList<SharedSessionMessageMetadata>();
    }
    this.previousMessageMetadata.add(elem);
  }

  @org.apache.thrift.annotation.Nullable
  public java.util.List<SharedSessionMessageMetadata> getPreviousMessageMetadata() {
    return this.previousMessageMetadata;
  }

  public SharedSessionMessage setPreviousMessageMetadata(@org.apache.thrift.annotation.Nullable java.util.List<SharedSessionMessageMetadata> previousMessageMetadata) {
    this.previousMessageMetadata = previousMessageMetadata;
    return this;
  }

  public void unsetPreviousMessageMetadata() {
    this.previousMessageMetadata = null;
  }

  /** Returns true if field previousMessageMetadata is set (has been assigned a value) and false otherwise */
  public boolean isSetPreviousMessageMetadata() {
    return this.previousMessageMetadata != null;
  }

  public void setPreviousMessageMetadataIsSet(boolean value) {
    if (!value) {
      this.previousMessageMetadata = null;
    }
  }

  @org.apache.thrift.annotation.Nullable
  public org.cweb.schemas.wire.TypedPayload getPayload() {
    return this.payload;
  }

  public SharedSessionMessage setPayload(@org.apache.thrift.annotation.Nullable org.cweb.schemas.wire.TypedPayload payload) {
    this.payload = payload;
    return this;
  }

  public void unsetPayload() {
    this.payload = null;
  }

  /** Returns true if field payload is set (has been assigned a value) and false otherwise */
  public boolean isSetPayload() {
    return this.payload != null;
  }

  public void setPayloadIsSet(boolean value) {
    if (!value) {
      this.payload = null;
    }
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case METADATA:
      if (value == null) {
        unsetMetadata();
      } else {
        setMetadata((SharedSessionMessageMetadata)value);
      }
      break;

    case PREVIOUS_MESSAGE_METADATA:
      if (value == null) {
        unsetPreviousMessageMetadata();
      } else {
        setPreviousMessageMetadata((java.util.List<SharedSessionMessageMetadata>)value);
      }
      break;

    case PAYLOAD:
      if (value == null) {
        unsetPayload();
      } else {
        setPayload((org.cweb.schemas.wire.TypedPayload)value);
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case METADATA:
      return getMetadata();

    case PREVIOUS_MESSAGE_METADATA:
      return getPreviousMessageMetadata();

    case PAYLOAD:
      return getPayload();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case METADATA:
      return isSetMetadata();
    case PREVIOUS_MESSAGE_METADATA:
      return isSetPreviousMessageMetadata();
    case PAYLOAD:
      return isSetPayload();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof SharedSessionMessage)
      return this.equals((SharedSessionMessage)that);
    return false;
  }

  public boolean equals(SharedSessionMessage that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_metadata = true && this.isSetMetadata();
    boolean that_present_metadata = true && that.isSetMetadata();
    if (this_present_metadata || that_present_metadata) {
      if (!(this_present_metadata && that_present_metadata))
        return false;
      if (!this.metadata.equals(that.metadata))
        return false;
    }

    boolean this_present_previousMessageMetadata = true && this.isSetPreviousMessageMetadata();
    boolean that_present_previousMessageMetadata = true && that.isSetPreviousMessageMetadata();
    if (this_present_previousMessageMetadata || that_present_previousMessageMetadata) {
      if (!(this_present_previousMessageMetadata && that_present_previousMessageMetadata))
        return false;
      if (!this.previousMessageMetadata.equals(that.previousMessageMetadata))
        return false;
    }

    boolean this_present_payload = true && this.isSetPayload();
    boolean that_present_payload = true && that.isSetPayload();
    if (this_present_payload || that_present_payload) {
      if (!(this_present_payload && that_present_payload))
        return false;
      if (!this.payload.equals(that.payload))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetMetadata()) ? 131071 : 524287);
    if (isSetMetadata())
      hashCode = hashCode * 8191 + metadata.hashCode();

    hashCode = hashCode * 8191 + ((isSetPreviousMessageMetadata()) ? 131071 : 524287);
    if (isSetPreviousMessageMetadata())
      hashCode = hashCode * 8191 + previousMessageMetadata.hashCode();

    hashCode = hashCode * 8191 + ((isSetPayload()) ? 131071 : 524287);
    if (isSetPayload())
      hashCode = hashCode * 8191 + payload.hashCode();

    return hashCode;
  }

  @Override
  public int compareTo(SharedSessionMessage other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetMetadata(), other.isSetMetadata());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMetadata()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.metadata, other.metadata);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetPreviousMessageMetadata(), other.isSetPreviousMessageMetadata());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPreviousMessageMetadata()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.previousMessageMetadata, other.previousMessageMetadata);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetPayload(), other.isSetPayload());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPayload()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.payload, other.payload);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("SharedSessionMessage(");
    boolean first = true;

    sb.append("metadata:");
    if (this.metadata == null) {
      sb.append("null");
    } else {
      sb.append(this.metadata);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("previousMessageMetadata:");
    if (this.previousMessageMetadata == null) {
      sb.append("null");
    } else {
      sb.append(this.previousMessageMetadata);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("payload:");
    if (this.payload == null) {
      sb.append("null");
    } else {
      sb.append(this.payload);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (metadata == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'metadata' was not present! Struct: " + toString());
    }
    if (previousMessageMetadata == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'previousMessageMetadata' was not present! Struct: " + toString());
    }
    if (payload == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'payload' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
    if (metadata != null) {
      metadata.validate();
    }
    if (payload != null) {
      payload.validate();
    }
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class SharedSessionMessageStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public SharedSessionMessageStandardScheme getScheme() {
      return new SharedSessionMessageStandardScheme();
    }
  }

  private static class SharedSessionMessageStandardScheme extends org.apache.thrift.scheme.StandardScheme<SharedSessionMessage> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, SharedSessionMessage struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // METADATA
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.metadata = new SharedSessionMessageMetadata();
              struct.metadata.read(iprot);
              struct.setMetadataIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // PREVIOUS_MESSAGE_METADATA
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list16 = iprot.readListBegin();
                struct.previousMessageMetadata = new java.util.ArrayList<SharedSessionMessageMetadata>(_list16.size);
                @org.apache.thrift.annotation.Nullable SharedSessionMessageMetadata _elem17;
                for (int _i18 = 0; _i18 < _list16.size; ++_i18)
                {
                  _elem17 = new SharedSessionMessageMetadata();
                  _elem17.read(iprot);
                  struct.previousMessageMetadata.add(_elem17);
                }
                iprot.readListEnd();
              }
              struct.setPreviousMessageMetadataIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 21: // PAYLOAD
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.payload = new org.cweb.schemas.wire.TypedPayload();
              struct.payload.read(iprot);
              struct.setPayloadIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, SharedSessionMessage struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.metadata != null) {
        oprot.writeFieldBegin(METADATA_FIELD_DESC);
        struct.metadata.write(oprot);
        oprot.writeFieldEnd();
      }
      if (struct.previousMessageMetadata != null) {
        oprot.writeFieldBegin(PREVIOUS_MESSAGE_METADATA_FIELD_DESC);
        {
          oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, struct.previousMessageMetadata.size()));
          for (SharedSessionMessageMetadata _iter19 : struct.previousMessageMetadata)
          {
            _iter19.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (struct.payload != null) {
        oprot.writeFieldBegin(PAYLOAD_FIELD_DESC);
        struct.payload.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class SharedSessionMessageTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public SharedSessionMessageTupleScheme getScheme() {
      return new SharedSessionMessageTupleScheme();
    }
  }

  private static class SharedSessionMessageTupleScheme extends org.apache.thrift.scheme.TupleScheme<SharedSessionMessage> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, SharedSessionMessage struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.metadata.write(oprot);
      {
        oprot.writeI32(struct.previousMessageMetadata.size());
        for (SharedSessionMessageMetadata _iter20 : struct.previousMessageMetadata)
        {
          _iter20.write(oprot);
        }
      }
      struct.payload.write(oprot);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, SharedSessionMessage struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.metadata = new SharedSessionMessageMetadata();
      struct.metadata.read(iprot);
      struct.setMetadataIsSet(true);
      {
        org.apache.thrift.protocol.TList _list21 = iprot.readListBegin(org.apache.thrift.protocol.TType.STRUCT);
        struct.previousMessageMetadata = new java.util.ArrayList<SharedSessionMessageMetadata>(_list21.size);
        @org.apache.thrift.annotation.Nullable SharedSessionMessageMetadata _elem22;
        for (int _i23 = 0; _i23 < _list21.size; ++_i23)
        {
          _elem22 = new SharedSessionMessageMetadata();
          _elem22.read(iprot);
          struct.previousMessageMetadata.add(_elem22);
        }
      }
      struct.setPreviousMessageMetadataIsSet(true);
      struct.payload = new org.cweb.schemas.wire.TypedPayload();
      struct.payload.read(iprot);
      struct.setPayloadIsSet(true);
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

