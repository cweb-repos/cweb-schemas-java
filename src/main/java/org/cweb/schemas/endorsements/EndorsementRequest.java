/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.endorsements;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class EndorsementRequest implements org.apache.thrift.TBase<EndorsementRequest, EndorsementRequest._Fields>, java.io.Serializable, Cloneable, Comparable<EndorsementRequest> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("EndorsementRequest");

  private static final org.apache.thrift.protocol.TField REQUESTER_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("requesterId", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField REQUEST_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("requestId", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField ENDORSEMENT_REQUEST_PAYLOAD_FIELD_DESC = new org.apache.thrift.protocol.TField("endorsementRequestPayload", org.apache.thrift.protocol.TType.STRING, (short)20);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new EndorsementRequestStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new EndorsementRequestTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer requesterId; // required
  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer requestId; // required
  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer endorsementRequestPayload; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    REQUESTER_ID((short)1, "requesterId"),
    REQUEST_ID((short)2, "requestId"),
    ENDORSEMENT_REQUEST_PAYLOAD((short)20, "endorsementRequestPayload");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // REQUESTER_ID
          return REQUESTER_ID;
        case 2: // REQUEST_ID
          return REQUEST_ID;
        case 20: // ENDORSEMENT_REQUEST_PAYLOAD
          return ENDORSEMENT_REQUEST_PAYLOAD;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.REQUESTER_ID, new org.apache.thrift.meta_data.FieldMetaData("requesterId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    tmpMap.put(_Fields.REQUEST_ID, new org.apache.thrift.meta_data.FieldMetaData("requestId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    tmpMap.put(_Fields.ENDORSEMENT_REQUEST_PAYLOAD, new org.apache.thrift.meta_data.FieldMetaData("endorsementRequestPayload", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(EndorsementRequest.class, metaDataMap);
  }

  public EndorsementRequest() {
  }

  public EndorsementRequest(
    java.nio.ByteBuffer requesterId,
    java.nio.ByteBuffer requestId,
    java.nio.ByteBuffer endorsementRequestPayload)
  {
    this();
    this.requesterId = org.apache.thrift.TBaseHelper.copyBinary(requesterId);
    this.requestId = org.apache.thrift.TBaseHelper.copyBinary(requestId);
    this.endorsementRequestPayload = org.apache.thrift.TBaseHelper.copyBinary(endorsementRequestPayload);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public EndorsementRequest(EndorsementRequest other) {
    if (other.isSetRequesterId()) {
      this.requesterId = org.apache.thrift.TBaseHelper.copyBinary(other.requesterId);
    }
    if (other.isSetRequestId()) {
      this.requestId = org.apache.thrift.TBaseHelper.copyBinary(other.requestId);
    }
    if (other.isSetEndorsementRequestPayload()) {
      this.endorsementRequestPayload = org.apache.thrift.TBaseHelper.copyBinary(other.endorsementRequestPayload);
    }
  }

  @Override
  public EndorsementRequest deepCopy() {
    return new EndorsementRequest(this);
  }

  @Override
  public void clear() {
    this.requesterId = null;
    this.requestId = null;
    this.endorsementRequestPayload = null;
  }

  public byte[] getRequesterId() {
    setRequesterId(org.apache.thrift.TBaseHelper.rightSize(requesterId));
    return requesterId == null ? null : requesterId.array();
  }

  public java.nio.ByteBuffer bufferForRequesterId() {
    return org.apache.thrift.TBaseHelper.copyBinary(requesterId);
  }

  public EndorsementRequest setRequesterId(byte[] requesterId) {
    this.requesterId = requesterId == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(requesterId.clone());
    return this;
  }

  public EndorsementRequest setRequesterId(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer requesterId) {
    this.requesterId = org.apache.thrift.TBaseHelper.copyBinary(requesterId);
    return this;
  }

  public void unsetRequesterId() {
    this.requesterId = null;
  }

  /** Returns true if field requesterId is set (has been assigned a value) and false otherwise */
  public boolean isSetRequesterId() {
    return this.requesterId != null;
  }

  public void setRequesterIdIsSet(boolean value) {
    if (!value) {
      this.requesterId = null;
    }
  }

  public byte[] getRequestId() {
    setRequestId(org.apache.thrift.TBaseHelper.rightSize(requestId));
    return requestId == null ? null : requestId.array();
  }

  public java.nio.ByteBuffer bufferForRequestId() {
    return org.apache.thrift.TBaseHelper.copyBinary(requestId);
  }

  public EndorsementRequest setRequestId(byte[] requestId) {
    this.requestId = requestId == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(requestId.clone());
    return this;
  }

  public EndorsementRequest setRequestId(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer requestId) {
    this.requestId = org.apache.thrift.TBaseHelper.copyBinary(requestId);
    return this;
  }

  public void unsetRequestId() {
    this.requestId = null;
  }

  /** Returns true if field requestId is set (has been assigned a value) and false otherwise */
  public boolean isSetRequestId() {
    return this.requestId != null;
  }

  public void setRequestIdIsSet(boolean value) {
    if (!value) {
      this.requestId = null;
    }
  }

  public byte[] getEndorsementRequestPayload() {
    setEndorsementRequestPayload(org.apache.thrift.TBaseHelper.rightSize(endorsementRequestPayload));
    return endorsementRequestPayload == null ? null : endorsementRequestPayload.array();
  }

  public java.nio.ByteBuffer bufferForEndorsementRequestPayload() {
    return org.apache.thrift.TBaseHelper.copyBinary(endorsementRequestPayload);
  }

  public EndorsementRequest setEndorsementRequestPayload(byte[] endorsementRequestPayload) {
    this.endorsementRequestPayload = endorsementRequestPayload == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(endorsementRequestPayload.clone());
    return this;
  }

  public EndorsementRequest setEndorsementRequestPayload(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer endorsementRequestPayload) {
    this.endorsementRequestPayload = org.apache.thrift.TBaseHelper.copyBinary(endorsementRequestPayload);
    return this;
  }

  public void unsetEndorsementRequestPayload() {
    this.endorsementRequestPayload = null;
  }

  /** Returns true if field endorsementRequestPayload is set (has been assigned a value) and false otherwise */
  public boolean isSetEndorsementRequestPayload() {
    return this.endorsementRequestPayload != null;
  }

  public void setEndorsementRequestPayloadIsSet(boolean value) {
    if (!value) {
      this.endorsementRequestPayload = null;
    }
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case REQUESTER_ID:
      if (value == null) {
        unsetRequesterId();
      } else {
        if (value instanceof byte[]) {
          setRequesterId((byte[])value);
        } else {
          setRequesterId((java.nio.ByteBuffer)value);
        }
      }
      break;

    case REQUEST_ID:
      if (value == null) {
        unsetRequestId();
      } else {
        if (value instanceof byte[]) {
          setRequestId((byte[])value);
        } else {
          setRequestId((java.nio.ByteBuffer)value);
        }
      }
      break;

    case ENDORSEMENT_REQUEST_PAYLOAD:
      if (value == null) {
        unsetEndorsementRequestPayload();
      } else {
        if (value instanceof byte[]) {
          setEndorsementRequestPayload((byte[])value);
        } else {
          setEndorsementRequestPayload((java.nio.ByteBuffer)value);
        }
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case REQUESTER_ID:
      return getRequesterId();

    case REQUEST_ID:
      return getRequestId();

    case ENDORSEMENT_REQUEST_PAYLOAD:
      return getEndorsementRequestPayload();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case REQUESTER_ID:
      return isSetRequesterId();
    case REQUEST_ID:
      return isSetRequestId();
    case ENDORSEMENT_REQUEST_PAYLOAD:
      return isSetEndorsementRequestPayload();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof EndorsementRequest)
      return this.equals((EndorsementRequest)that);
    return false;
  }

  public boolean equals(EndorsementRequest that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_requesterId = true && this.isSetRequesterId();
    boolean that_present_requesterId = true && that.isSetRequesterId();
    if (this_present_requesterId || that_present_requesterId) {
      if (!(this_present_requesterId && that_present_requesterId))
        return false;
      if (!this.requesterId.equals(that.requesterId))
        return false;
    }

    boolean this_present_requestId = true && this.isSetRequestId();
    boolean that_present_requestId = true && that.isSetRequestId();
    if (this_present_requestId || that_present_requestId) {
      if (!(this_present_requestId && that_present_requestId))
        return false;
      if (!this.requestId.equals(that.requestId))
        return false;
    }

    boolean this_present_endorsementRequestPayload = true && this.isSetEndorsementRequestPayload();
    boolean that_present_endorsementRequestPayload = true && that.isSetEndorsementRequestPayload();
    if (this_present_endorsementRequestPayload || that_present_endorsementRequestPayload) {
      if (!(this_present_endorsementRequestPayload && that_present_endorsementRequestPayload))
        return false;
      if (!this.endorsementRequestPayload.equals(that.endorsementRequestPayload))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetRequesterId()) ? 131071 : 524287);
    if (isSetRequesterId())
      hashCode = hashCode * 8191 + requesterId.hashCode();

    hashCode = hashCode * 8191 + ((isSetRequestId()) ? 131071 : 524287);
    if (isSetRequestId())
      hashCode = hashCode * 8191 + requestId.hashCode();

    hashCode = hashCode * 8191 + ((isSetEndorsementRequestPayload()) ? 131071 : 524287);
    if (isSetEndorsementRequestPayload())
      hashCode = hashCode * 8191 + endorsementRequestPayload.hashCode();

    return hashCode;
  }

  @Override
  public int compareTo(EndorsementRequest other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetRequesterId(), other.isSetRequesterId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRequesterId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.requesterId, other.requesterId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetRequestId(), other.isSetRequestId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRequestId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.requestId, other.requestId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetEndorsementRequestPayload(), other.isSetEndorsementRequestPayload());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetEndorsementRequestPayload()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.endorsementRequestPayload, other.endorsementRequestPayload);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("EndorsementRequest(");
    boolean first = true;

    sb.append("requesterId:");
    if (this.requesterId == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.requesterId, sb);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("requestId:");
    if (this.requestId == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.requestId, sb);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("endorsementRequestPayload:");
    if (this.endorsementRequestPayload == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.endorsementRequestPayload, sb);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (requesterId == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'requesterId' was not present! Struct: " + toString());
    }
    if (requestId == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'requestId' was not present! Struct: " + toString());
    }
    if (endorsementRequestPayload == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'endorsementRequestPayload' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class EndorsementRequestStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public EndorsementRequestStandardScheme getScheme() {
      return new EndorsementRequestStandardScheme();
    }
  }

  private static class EndorsementRequestStandardScheme extends org.apache.thrift.scheme.StandardScheme<EndorsementRequest> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, EndorsementRequest struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // REQUESTER_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.requesterId = iprot.readBinary();
              struct.setRequesterIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // REQUEST_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.requestId = iprot.readBinary();
              struct.setRequestIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 20: // ENDORSEMENT_REQUEST_PAYLOAD
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.endorsementRequestPayload = iprot.readBinary();
              struct.setEndorsementRequestPayloadIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, EndorsementRequest struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.requesterId != null) {
        oprot.writeFieldBegin(REQUESTER_ID_FIELD_DESC);
        oprot.writeBinary(struct.requesterId);
        oprot.writeFieldEnd();
      }
      if (struct.requestId != null) {
        oprot.writeFieldBegin(REQUEST_ID_FIELD_DESC);
        oprot.writeBinary(struct.requestId);
        oprot.writeFieldEnd();
      }
      if (struct.endorsementRequestPayload != null) {
        oprot.writeFieldBegin(ENDORSEMENT_REQUEST_PAYLOAD_FIELD_DESC);
        oprot.writeBinary(struct.endorsementRequestPayload);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class EndorsementRequestTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public EndorsementRequestTupleScheme getScheme() {
      return new EndorsementRequestTupleScheme();
    }
  }

  private static class EndorsementRequestTupleScheme extends org.apache.thrift.scheme.TupleScheme<EndorsementRequest> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, EndorsementRequest struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      oprot.writeBinary(struct.requesterId);
      oprot.writeBinary(struct.requestId);
      oprot.writeBinary(struct.endorsementRequestPayload);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, EndorsementRequest struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.requesterId = iprot.readBinary();
      struct.setRequesterIdIsSet(true);
      struct.requestId = iprot.readBinary();
      struct.setRequestIdIsSet(true);
      struct.endorsementRequestPayload = iprot.readBinary();
      struct.setEndorsementRequestPayloadIsSet(true);
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

