/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.storage;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class RemoteSyncStatus implements org.apache.thrift.TBase<RemoteSyncStatus, RemoteSyncStatus._Fields>, java.io.Serializable, Cloneable, Comparable<RemoteSyncStatus> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("RemoteSyncStatus");

  private static final org.apache.thrift.protocol.TField LAST_SUCCESS_TIME_FIELD_DESC = new org.apache.thrift.protocol.TField("lastSuccessTime", org.apache.thrift.protocol.TType.I64, (short)1);
  private static final org.apache.thrift.protocol.TField LAST_ATTEMPT_TIME_FIELD_DESC = new org.apache.thrift.protocol.TField("lastAttemptTime", org.apache.thrift.protocol.TType.I64, (short)2);
  private static final org.apache.thrift.protocol.TField LAST_ERROR_FIELD_DESC = new org.apache.thrift.protocol.TField("lastError", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField NUM_CONSECUTIVE_ERRORS_FIELD_DESC = new org.apache.thrift.protocol.TField("numConsecutiveErrors", org.apache.thrift.protocol.TType.I32, (short)4);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new RemoteSyncStatusStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new RemoteSyncStatusTupleSchemeFactory();

  public long lastSuccessTime; // optional
  public long lastAttemptTime; // optional
  public @org.apache.thrift.annotation.Nullable java.lang.String lastError; // optional
  public int numConsecutiveErrors; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    LAST_SUCCESS_TIME((short)1, "lastSuccessTime"),
    LAST_ATTEMPT_TIME((short)2, "lastAttemptTime"),
    LAST_ERROR((short)3, "lastError"),
    NUM_CONSECUTIVE_ERRORS((short)4, "numConsecutiveErrors");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // LAST_SUCCESS_TIME
          return LAST_SUCCESS_TIME;
        case 2: // LAST_ATTEMPT_TIME
          return LAST_ATTEMPT_TIME;
        case 3: // LAST_ERROR
          return LAST_ERROR;
        case 4: // NUM_CONSECUTIVE_ERRORS
          return NUM_CONSECUTIVE_ERRORS;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __LASTSUCCESSTIME_ISSET_ID = 0;
  private static final int __LASTATTEMPTTIME_ISSET_ID = 1;
  private static final int __NUMCONSECUTIVEERRORS_ISSET_ID = 2;
  private byte __isset_bitfield = 0;
  private static final _Fields optionals[] = {_Fields.LAST_SUCCESS_TIME,_Fields.LAST_ATTEMPT_TIME,_Fields.LAST_ERROR};
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.LAST_SUCCESS_TIME, new org.apache.thrift.meta_data.FieldMetaData("lastSuccessTime", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.LAST_ATTEMPT_TIME, new org.apache.thrift.meta_data.FieldMetaData("lastAttemptTime", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.LAST_ERROR, new org.apache.thrift.meta_data.FieldMetaData("lastError", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.NUM_CONSECUTIVE_ERRORS, new org.apache.thrift.meta_data.FieldMetaData("numConsecutiveErrors", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(RemoteSyncStatus.class, metaDataMap);
  }

  public RemoteSyncStatus() {
    this.numConsecutiveErrors = 0;

  }

  public RemoteSyncStatus(
    int numConsecutiveErrors)
  {
    this();
    this.numConsecutiveErrors = numConsecutiveErrors;
    setNumConsecutiveErrorsIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public RemoteSyncStatus(RemoteSyncStatus other) {
    __isset_bitfield = other.__isset_bitfield;
    this.lastSuccessTime = other.lastSuccessTime;
    this.lastAttemptTime = other.lastAttemptTime;
    if (other.isSetLastError()) {
      this.lastError = other.lastError;
    }
    this.numConsecutiveErrors = other.numConsecutiveErrors;
  }

  @Override
  public RemoteSyncStatus deepCopy() {
    return new RemoteSyncStatus(this);
  }

  @Override
  public void clear() {
    setLastSuccessTimeIsSet(false);
    this.lastSuccessTime = 0;
    setLastAttemptTimeIsSet(false);
    this.lastAttemptTime = 0;
    this.lastError = null;
    this.numConsecutiveErrors = 0;

  }

  public long getLastSuccessTime() {
    return this.lastSuccessTime;
  }

  public RemoteSyncStatus setLastSuccessTime(long lastSuccessTime) {
    this.lastSuccessTime = lastSuccessTime;
    setLastSuccessTimeIsSet(true);
    return this;
  }

  public void unsetLastSuccessTime() {
    __isset_bitfield = org.apache.thrift.EncodingUtils.clearBit(__isset_bitfield, __LASTSUCCESSTIME_ISSET_ID);
  }

  /** Returns true if field lastSuccessTime is set (has been assigned a value) and false otherwise */
  public boolean isSetLastSuccessTime() {
    return org.apache.thrift.EncodingUtils.testBit(__isset_bitfield, __LASTSUCCESSTIME_ISSET_ID);
  }

  public void setLastSuccessTimeIsSet(boolean value) {
    __isset_bitfield = org.apache.thrift.EncodingUtils.setBit(__isset_bitfield, __LASTSUCCESSTIME_ISSET_ID, value);
  }

  public long getLastAttemptTime() {
    return this.lastAttemptTime;
  }

  public RemoteSyncStatus setLastAttemptTime(long lastAttemptTime) {
    this.lastAttemptTime = lastAttemptTime;
    setLastAttemptTimeIsSet(true);
    return this;
  }

  public void unsetLastAttemptTime() {
    __isset_bitfield = org.apache.thrift.EncodingUtils.clearBit(__isset_bitfield, __LASTATTEMPTTIME_ISSET_ID);
  }

  /** Returns true if field lastAttemptTime is set (has been assigned a value) and false otherwise */
  public boolean isSetLastAttemptTime() {
    return org.apache.thrift.EncodingUtils.testBit(__isset_bitfield, __LASTATTEMPTTIME_ISSET_ID);
  }

  public void setLastAttemptTimeIsSet(boolean value) {
    __isset_bitfield = org.apache.thrift.EncodingUtils.setBit(__isset_bitfield, __LASTATTEMPTTIME_ISSET_ID, value);
  }

  @org.apache.thrift.annotation.Nullable
  public java.lang.String getLastError() {
    return this.lastError;
  }

  public RemoteSyncStatus setLastError(@org.apache.thrift.annotation.Nullable java.lang.String lastError) {
    this.lastError = lastError;
    return this;
  }

  public void unsetLastError() {
    this.lastError = null;
  }

  /** Returns true if field lastError is set (has been assigned a value) and false otherwise */
  public boolean isSetLastError() {
    return this.lastError != null;
  }

  public void setLastErrorIsSet(boolean value) {
    if (!value) {
      this.lastError = null;
    }
  }

  public int getNumConsecutiveErrors() {
    return this.numConsecutiveErrors;
  }

  public RemoteSyncStatus setNumConsecutiveErrors(int numConsecutiveErrors) {
    this.numConsecutiveErrors = numConsecutiveErrors;
    setNumConsecutiveErrorsIsSet(true);
    return this;
  }

  public void unsetNumConsecutiveErrors() {
    __isset_bitfield = org.apache.thrift.EncodingUtils.clearBit(__isset_bitfield, __NUMCONSECUTIVEERRORS_ISSET_ID);
  }

  /** Returns true if field numConsecutiveErrors is set (has been assigned a value) and false otherwise */
  public boolean isSetNumConsecutiveErrors() {
    return org.apache.thrift.EncodingUtils.testBit(__isset_bitfield, __NUMCONSECUTIVEERRORS_ISSET_ID);
  }

  public void setNumConsecutiveErrorsIsSet(boolean value) {
    __isset_bitfield = org.apache.thrift.EncodingUtils.setBit(__isset_bitfield, __NUMCONSECUTIVEERRORS_ISSET_ID, value);
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case LAST_SUCCESS_TIME:
      if (value == null) {
        unsetLastSuccessTime();
      } else {
        setLastSuccessTime((java.lang.Long)value);
      }
      break;

    case LAST_ATTEMPT_TIME:
      if (value == null) {
        unsetLastAttemptTime();
      } else {
        setLastAttemptTime((java.lang.Long)value);
      }
      break;

    case LAST_ERROR:
      if (value == null) {
        unsetLastError();
      } else {
        setLastError((java.lang.String)value);
      }
      break;

    case NUM_CONSECUTIVE_ERRORS:
      if (value == null) {
        unsetNumConsecutiveErrors();
      } else {
        setNumConsecutiveErrors((java.lang.Integer)value);
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case LAST_SUCCESS_TIME:
      return getLastSuccessTime();

    case LAST_ATTEMPT_TIME:
      return getLastAttemptTime();

    case LAST_ERROR:
      return getLastError();

    case NUM_CONSECUTIVE_ERRORS:
      return getNumConsecutiveErrors();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case LAST_SUCCESS_TIME:
      return isSetLastSuccessTime();
    case LAST_ATTEMPT_TIME:
      return isSetLastAttemptTime();
    case LAST_ERROR:
      return isSetLastError();
    case NUM_CONSECUTIVE_ERRORS:
      return isSetNumConsecutiveErrors();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof RemoteSyncStatus)
      return this.equals((RemoteSyncStatus)that);
    return false;
  }

  public boolean equals(RemoteSyncStatus that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_lastSuccessTime = true && this.isSetLastSuccessTime();
    boolean that_present_lastSuccessTime = true && that.isSetLastSuccessTime();
    if (this_present_lastSuccessTime || that_present_lastSuccessTime) {
      if (!(this_present_lastSuccessTime && that_present_lastSuccessTime))
        return false;
      if (this.lastSuccessTime != that.lastSuccessTime)
        return false;
    }

    boolean this_present_lastAttemptTime = true && this.isSetLastAttemptTime();
    boolean that_present_lastAttemptTime = true && that.isSetLastAttemptTime();
    if (this_present_lastAttemptTime || that_present_lastAttemptTime) {
      if (!(this_present_lastAttemptTime && that_present_lastAttemptTime))
        return false;
      if (this.lastAttemptTime != that.lastAttemptTime)
        return false;
    }

    boolean this_present_lastError = true && this.isSetLastError();
    boolean that_present_lastError = true && that.isSetLastError();
    if (this_present_lastError || that_present_lastError) {
      if (!(this_present_lastError && that_present_lastError))
        return false;
      if (!this.lastError.equals(that.lastError))
        return false;
    }

    boolean this_present_numConsecutiveErrors = true;
    boolean that_present_numConsecutiveErrors = true;
    if (this_present_numConsecutiveErrors || that_present_numConsecutiveErrors) {
      if (!(this_present_numConsecutiveErrors && that_present_numConsecutiveErrors))
        return false;
      if (this.numConsecutiveErrors != that.numConsecutiveErrors)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetLastSuccessTime()) ? 131071 : 524287);
    if (isSetLastSuccessTime())
      hashCode = hashCode * 8191 + org.apache.thrift.TBaseHelper.hashCode(lastSuccessTime);

    hashCode = hashCode * 8191 + ((isSetLastAttemptTime()) ? 131071 : 524287);
    if (isSetLastAttemptTime())
      hashCode = hashCode * 8191 + org.apache.thrift.TBaseHelper.hashCode(lastAttemptTime);

    hashCode = hashCode * 8191 + ((isSetLastError()) ? 131071 : 524287);
    if (isSetLastError())
      hashCode = hashCode * 8191 + lastError.hashCode();

    hashCode = hashCode * 8191 + numConsecutiveErrors;

    return hashCode;
  }

  @Override
  public int compareTo(RemoteSyncStatus other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetLastSuccessTime(), other.isSetLastSuccessTime());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLastSuccessTime()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.lastSuccessTime, other.lastSuccessTime);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetLastAttemptTime(), other.isSetLastAttemptTime());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLastAttemptTime()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.lastAttemptTime, other.lastAttemptTime);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetLastError(), other.isSetLastError());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLastError()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.lastError, other.lastError);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetNumConsecutiveErrors(), other.isSetNumConsecutiveErrors());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetNumConsecutiveErrors()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.numConsecutiveErrors, other.numConsecutiveErrors);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("RemoteSyncStatus(");
    boolean first = true;

    if (isSetLastSuccessTime()) {
      sb.append("lastSuccessTime:");
      sb.append(this.lastSuccessTime);
      first = false;
    }
    if (isSetLastAttemptTime()) {
      if (!first) sb.append(", ");
      sb.append("lastAttemptTime:");
      sb.append(this.lastAttemptTime);
      first = false;
    }
    if (isSetLastError()) {
      if (!first) sb.append(", ");
      sb.append("lastError:");
      if (this.lastError == null) {
        sb.append("null");
      } else {
        sb.append(this.lastError);
      }
      first = false;
    }
    if (!first) sb.append(", ");
    sb.append("numConsecutiveErrors:");
    sb.append(this.numConsecutiveErrors);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // alas, we cannot check 'numConsecutiveErrors' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class RemoteSyncStatusStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public RemoteSyncStatusStandardScheme getScheme() {
      return new RemoteSyncStatusStandardScheme();
    }
  }

  private static class RemoteSyncStatusStandardScheme extends org.apache.thrift.scheme.StandardScheme<RemoteSyncStatus> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, RemoteSyncStatus struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // LAST_SUCCESS_TIME
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.lastSuccessTime = iprot.readI64();
              struct.setLastSuccessTimeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // LAST_ATTEMPT_TIME
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.lastAttemptTime = iprot.readI64();
              struct.setLastAttemptTimeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // LAST_ERROR
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.lastError = iprot.readString();
              struct.setLastErrorIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // NUM_CONSECUTIVE_ERRORS
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.numConsecutiveErrors = iprot.readI32();
              struct.setNumConsecutiveErrorsIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetNumConsecutiveErrors()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'numConsecutiveErrors' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, RemoteSyncStatus struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.isSetLastSuccessTime()) {
        oprot.writeFieldBegin(LAST_SUCCESS_TIME_FIELD_DESC);
        oprot.writeI64(struct.lastSuccessTime);
        oprot.writeFieldEnd();
      }
      if (struct.isSetLastAttemptTime()) {
        oprot.writeFieldBegin(LAST_ATTEMPT_TIME_FIELD_DESC);
        oprot.writeI64(struct.lastAttemptTime);
        oprot.writeFieldEnd();
      }
      if (struct.lastError != null) {
        if (struct.isSetLastError()) {
          oprot.writeFieldBegin(LAST_ERROR_FIELD_DESC);
          oprot.writeString(struct.lastError);
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldBegin(NUM_CONSECUTIVE_ERRORS_FIELD_DESC);
      oprot.writeI32(struct.numConsecutiveErrors);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class RemoteSyncStatusTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public RemoteSyncStatusTupleScheme getScheme() {
      return new RemoteSyncStatusTupleScheme();
    }
  }

  private static class RemoteSyncStatusTupleScheme extends org.apache.thrift.scheme.TupleScheme<RemoteSyncStatus> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, RemoteSyncStatus struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      oprot.writeI32(struct.numConsecutiveErrors);
      java.util.BitSet optionals = new java.util.BitSet();
      if (struct.isSetLastSuccessTime()) {
        optionals.set(0);
      }
      if (struct.isSetLastAttemptTime()) {
        optionals.set(1);
      }
      if (struct.isSetLastError()) {
        optionals.set(2);
      }
      oprot.writeBitSet(optionals, 3);
      if (struct.isSetLastSuccessTime()) {
        oprot.writeI64(struct.lastSuccessTime);
      }
      if (struct.isSetLastAttemptTime()) {
        oprot.writeI64(struct.lastAttemptTime);
      }
      if (struct.isSetLastError()) {
        oprot.writeString(struct.lastError);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, RemoteSyncStatus struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.numConsecutiveErrors = iprot.readI32();
      struct.setNumConsecutiveErrorsIsSet(true);
      java.util.BitSet incoming = iprot.readBitSet(3);
      if (incoming.get(0)) {
        struct.lastSuccessTime = iprot.readI64();
        struct.setLastSuccessTimeIsSet(true);
      }
      if (incoming.get(1)) {
        struct.lastAttemptTime = iprot.readI64();
        struct.setLastAttemptTimeIsSet(true);
      }
      if (incoming.get(2)) {
        struct.lastError = iprot.readString();
        struct.setLastErrorIsSet(true);
      }
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

