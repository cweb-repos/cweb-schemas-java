/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.wire;


@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public enum CompressionType implements org.apache.thrift.TEnum {
  NONE(0),
  GZIP(1),
  DEFLATE(2);

  private final int value;

  private CompressionType(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  @Override
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  @org.apache.thrift.annotation.Nullable
  public static CompressionType findByValue(int value) { 
    switch (value) {
      case 0:
        return NONE;
      case 1:
        return GZIP;
      case 2:
        return DEFLATE;
      default:
        return null;
    }
  }
}
