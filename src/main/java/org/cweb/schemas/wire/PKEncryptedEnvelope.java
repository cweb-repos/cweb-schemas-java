/**
 * Autogenerated by Thrift Compiler (0.19.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.cweb.schemas.wire;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.19.0)")
public class PKEncryptedEnvelope implements org.apache.thrift.TBase<PKEncryptedEnvelope, PKEncryptedEnvelope._Fields>, java.io.Serializable, Cloneable, Comparable<PKEncryptedEnvelope> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("PKEncryptedEnvelope");

  private static final org.apache.thrift.protocol.TField PK_ENCRYPTED_KEY_FIELD_DESC = new org.apache.thrift.protocol.TField("pkEncryptedKey", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField KEY_ENCRYPTION_ALGORITHM_FIELD_DESC = new org.apache.thrift.protocol.TField("keyEncryptionAlgorithm", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField PAYLOAD_ENCRYPTION_ALGORITHM_FIELD_DESC = new org.apache.thrift.protocol.TField("payloadEncryptionAlgorithm", org.apache.thrift.protocol.TType.STRING, (short)4);
  private static final org.apache.thrift.protocol.TField PAYLOAD_FIELD_DESC = new org.apache.thrift.protocol.TField("payload", org.apache.thrift.protocol.TType.STRING, (short)20);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new PKEncryptedEnvelopeStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new PKEncryptedEnvelopeTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer pkEncryptedKey; // required
  public @org.apache.thrift.annotation.Nullable java.lang.String keyEncryptionAlgorithm; // required
  public @org.apache.thrift.annotation.Nullable java.lang.String payloadEncryptionAlgorithm; // required
  public @org.apache.thrift.annotation.Nullable java.nio.ByteBuffer payload; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    PK_ENCRYPTED_KEY((short)1, "pkEncryptedKey"),
    KEY_ENCRYPTION_ALGORITHM((short)3, "keyEncryptionAlgorithm"),
    PAYLOAD_ENCRYPTION_ALGORITHM((short)4, "payloadEncryptionAlgorithm"),
    PAYLOAD((short)20, "payload");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // PK_ENCRYPTED_KEY
          return PK_ENCRYPTED_KEY;
        case 3: // KEY_ENCRYPTION_ALGORITHM
          return KEY_ENCRYPTION_ALGORITHM;
        case 4: // PAYLOAD_ENCRYPTION_ALGORITHM
          return PAYLOAD_ENCRYPTION_ALGORITHM;
        case 20: // PAYLOAD
          return PAYLOAD;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    @Override
    public short getThriftFieldId() {
      return _thriftId;
    }

    @Override
    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.PK_ENCRYPTED_KEY, new org.apache.thrift.meta_data.FieldMetaData("pkEncryptedKey", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    tmpMap.put(_Fields.KEY_ENCRYPTION_ALGORITHM, new org.apache.thrift.meta_data.FieldMetaData("keyEncryptionAlgorithm", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.PAYLOAD_ENCRYPTION_ALGORITHM, new org.apache.thrift.meta_data.FieldMetaData("payloadEncryptionAlgorithm", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.PAYLOAD, new org.apache.thrift.meta_data.FieldMetaData("payload", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING        , true)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(PKEncryptedEnvelope.class, metaDataMap);
  }

  public PKEncryptedEnvelope() {
  }

  public PKEncryptedEnvelope(
    java.nio.ByteBuffer pkEncryptedKey,
    java.lang.String keyEncryptionAlgorithm,
    java.lang.String payloadEncryptionAlgorithm,
    java.nio.ByteBuffer payload)
  {
    this();
    this.pkEncryptedKey = org.apache.thrift.TBaseHelper.copyBinary(pkEncryptedKey);
    this.keyEncryptionAlgorithm = keyEncryptionAlgorithm;
    this.payloadEncryptionAlgorithm = payloadEncryptionAlgorithm;
    this.payload = org.apache.thrift.TBaseHelper.copyBinary(payload);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public PKEncryptedEnvelope(PKEncryptedEnvelope other) {
    if (other.isSetPkEncryptedKey()) {
      this.pkEncryptedKey = org.apache.thrift.TBaseHelper.copyBinary(other.pkEncryptedKey);
    }
    if (other.isSetKeyEncryptionAlgorithm()) {
      this.keyEncryptionAlgorithm = other.keyEncryptionAlgorithm;
    }
    if (other.isSetPayloadEncryptionAlgorithm()) {
      this.payloadEncryptionAlgorithm = other.payloadEncryptionAlgorithm;
    }
    if (other.isSetPayload()) {
      this.payload = org.apache.thrift.TBaseHelper.copyBinary(other.payload);
    }
  }

  @Override
  public PKEncryptedEnvelope deepCopy() {
    return new PKEncryptedEnvelope(this);
  }

  @Override
  public void clear() {
    this.pkEncryptedKey = null;
    this.keyEncryptionAlgorithm = null;
    this.payloadEncryptionAlgorithm = null;
    this.payload = null;
  }

  public byte[] getPkEncryptedKey() {
    setPkEncryptedKey(org.apache.thrift.TBaseHelper.rightSize(pkEncryptedKey));
    return pkEncryptedKey == null ? null : pkEncryptedKey.array();
  }

  public java.nio.ByteBuffer bufferForPkEncryptedKey() {
    return org.apache.thrift.TBaseHelper.copyBinary(pkEncryptedKey);
  }

  public PKEncryptedEnvelope setPkEncryptedKey(byte[] pkEncryptedKey) {
    this.pkEncryptedKey = pkEncryptedKey == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(pkEncryptedKey.clone());
    return this;
  }

  public PKEncryptedEnvelope setPkEncryptedKey(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer pkEncryptedKey) {
    this.pkEncryptedKey = org.apache.thrift.TBaseHelper.copyBinary(pkEncryptedKey);
    return this;
  }

  public void unsetPkEncryptedKey() {
    this.pkEncryptedKey = null;
  }

  /** Returns true if field pkEncryptedKey is set (has been assigned a value) and false otherwise */
  public boolean isSetPkEncryptedKey() {
    return this.pkEncryptedKey != null;
  }

  public void setPkEncryptedKeyIsSet(boolean value) {
    if (!value) {
      this.pkEncryptedKey = null;
    }
  }

  @org.apache.thrift.annotation.Nullable
  public java.lang.String getKeyEncryptionAlgorithm() {
    return this.keyEncryptionAlgorithm;
  }

  public PKEncryptedEnvelope setKeyEncryptionAlgorithm(@org.apache.thrift.annotation.Nullable java.lang.String keyEncryptionAlgorithm) {
    this.keyEncryptionAlgorithm = keyEncryptionAlgorithm;
    return this;
  }

  public void unsetKeyEncryptionAlgorithm() {
    this.keyEncryptionAlgorithm = null;
  }

  /** Returns true if field keyEncryptionAlgorithm is set (has been assigned a value) and false otherwise */
  public boolean isSetKeyEncryptionAlgorithm() {
    return this.keyEncryptionAlgorithm != null;
  }

  public void setKeyEncryptionAlgorithmIsSet(boolean value) {
    if (!value) {
      this.keyEncryptionAlgorithm = null;
    }
  }

  @org.apache.thrift.annotation.Nullable
  public java.lang.String getPayloadEncryptionAlgorithm() {
    return this.payloadEncryptionAlgorithm;
  }

  public PKEncryptedEnvelope setPayloadEncryptionAlgorithm(@org.apache.thrift.annotation.Nullable java.lang.String payloadEncryptionAlgorithm) {
    this.payloadEncryptionAlgorithm = payloadEncryptionAlgorithm;
    return this;
  }

  public void unsetPayloadEncryptionAlgorithm() {
    this.payloadEncryptionAlgorithm = null;
  }

  /** Returns true if field payloadEncryptionAlgorithm is set (has been assigned a value) and false otherwise */
  public boolean isSetPayloadEncryptionAlgorithm() {
    return this.payloadEncryptionAlgorithm != null;
  }

  public void setPayloadEncryptionAlgorithmIsSet(boolean value) {
    if (!value) {
      this.payloadEncryptionAlgorithm = null;
    }
  }

  public byte[] getPayload() {
    setPayload(org.apache.thrift.TBaseHelper.rightSize(payload));
    return payload == null ? null : payload.array();
  }

  public java.nio.ByteBuffer bufferForPayload() {
    return org.apache.thrift.TBaseHelper.copyBinary(payload);
  }

  public PKEncryptedEnvelope setPayload(byte[] payload) {
    this.payload = payload == null ? (java.nio.ByteBuffer)null   : java.nio.ByteBuffer.wrap(payload.clone());
    return this;
  }

  public PKEncryptedEnvelope setPayload(@org.apache.thrift.annotation.Nullable java.nio.ByteBuffer payload) {
    this.payload = org.apache.thrift.TBaseHelper.copyBinary(payload);
    return this;
  }

  public void unsetPayload() {
    this.payload = null;
  }

  /** Returns true if field payload is set (has been assigned a value) and false otherwise */
  public boolean isSetPayload() {
    return this.payload != null;
  }

  public void setPayloadIsSet(boolean value) {
    if (!value) {
      this.payload = null;
    }
  }

  @Override
  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case PK_ENCRYPTED_KEY:
      if (value == null) {
        unsetPkEncryptedKey();
      } else {
        if (value instanceof byte[]) {
          setPkEncryptedKey((byte[])value);
        } else {
          setPkEncryptedKey((java.nio.ByteBuffer)value);
        }
      }
      break;

    case KEY_ENCRYPTION_ALGORITHM:
      if (value == null) {
        unsetKeyEncryptionAlgorithm();
      } else {
        setKeyEncryptionAlgorithm((java.lang.String)value);
      }
      break;

    case PAYLOAD_ENCRYPTION_ALGORITHM:
      if (value == null) {
        unsetPayloadEncryptionAlgorithm();
      } else {
        setPayloadEncryptionAlgorithm((java.lang.String)value);
      }
      break;

    case PAYLOAD:
      if (value == null) {
        unsetPayload();
      } else {
        if (value instanceof byte[]) {
          setPayload((byte[])value);
        } else {
          setPayload((java.nio.ByteBuffer)value);
        }
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case PK_ENCRYPTED_KEY:
      return getPkEncryptedKey();

    case KEY_ENCRYPTION_ALGORITHM:
      return getKeyEncryptionAlgorithm();

    case PAYLOAD_ENCRYPTION_ALGORITHM:
      return getPayloadEncryptionAlgorithm();

    case PAYLOAD:
      return getPayload();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  @Override
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case PK_ENCRYPTED_KEY:
      return isSetPkEncryptedKey();
    case KEY_ENCRYPTION_ALGORITHM:
      return isSetKeyEncryptionAlgorithm();
    case PAYLOAD_ENCRYPTION_ALGORITHM:
      return isSetPayloadEncryptionAlgorithm();
    case PAYLOAD:
      return isSetPayload();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that instanceof PKEncryptedEnvelope)
      return this.equals((PKEncryptedEnvelope)that);
    return false;
  }

  public boolean equals(PKEncryptedEnvelope that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_pkEncryptedKey = true && this.isSetPkEncryptedKey();
    boolean that_present_pkEncryptedKey = true && that.isSetPkEncryptedKey();
    if (this_present_pkEncryptedKey || that_present_pkEncryptedKey) {
      if (!(this_present_pkEncryptedKey && that_present_pkEncryptedKey))
        return false;
      if (!this.pkEncryptedKey.equals(that.pkEncryptedKey))
        return false;
    }

    boolean this_present_keyEncryptionAlgorithm = true && this.isSetKeyEncryptionAlgorithm();
    boolean that_present_keyEncryptionAlgorithm = true && that.isSetKeyEncryptionAlgorithm();
    if (this_present_keyEncryptionAlgorithm || that_present_keyEncryptionAlgorithm) {
      if (!(this_present_keyEncryptionAlgorithm && that_present_keyEncryptionAlgorithm))
        return false;
      if (!this.keyEncryptionAlgorithm.equals(that.keyEncryptionAlgorithm))
        return false;
    }

    boolean this_present_payloadEncryptionAlgorithm = true && this.isSetPayloadEncryptionAlgorithm();
    boolean that_present_payloadEncryptionAlgorithm = true && that.isSetPayloadEncryptionAlgorithm();
    if (this_present_payloadEncryptionAlgorithm || that_present_payloadEncryptionAlgorithm) {
      if (!(this_present_payloadEncryptionAlgorithm && that_present_payloadEncryptionAlgorithm))
        return false;
      if (!this.payloadEncryptionAlgorithm.equals(that.payloadEncryptionAlgorithm))
        return false;
    }

    boolean this_present_payload = true && this.isSetPayload();
    boolean that_present_payload = true && that.isSetPayload();
    if (this_present_payload || that_present_payload) {
      if (!(this_present_payload && that_present_payload))
        return false;
      if (!this.payload.equals(that.payload))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetPkEncryptedKey()) ? 131071 : 524287);
    if (isSetPkEncryptedKey())
      hashCode = hashCode * 8191 + pkEncryptedKey.hashCode();

    hashCode = hashCode * 8191 + ((isSetKeyEncryptionAlgorithm()) ? 131071 : 524287);
    if (isSetKeyEncryptionAlgorithm())
      hashCode = hashCode * 8191 + keyEncryptionAlgorithm.hashCode();

    hashCode = hashCode * 8191 + ((isSetPayloadEncryptionAlgorithm()) ? 131071 : 524287);
    if (isSetPayloadEncryptionAlgorithm())
      hashCode = hashCode * 8191 + payloadEncryptionAlgorithm.hashCode();

    hashCode = hashCode * 8191 + ((isSetPayload()) ? 131071 : 524287);
    if (isSetPayload())
      hashCode = hashCode * 8191 + payload.hashCode();

    return hashCode;
  }

  @Override
  public int compareTo(PKEncryptedEnvelope other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.compare(isSetPkEncryptedKey(), other.isSetPkEncryptedKey());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPkEncryptedKey()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.pkEncryptedKey, other.pkEncryptedKey);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetKeyEncryptionAlgorithm(), other.isSetKeyEncryptionAlgorithm());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetKeyEncryptionAlgorithm()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.keyEncryptionAlgorithm, other.keyEncryptionAlgorithm);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetPayloadEncryptionAlgorithm(), other.isSetPayloadEncryptionAlgorithm());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPayloadEncryptionAlgorithm()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.payloadEncryptionAlgorithm, other.payloadEncryptionAlgorithm);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.compare(isSetPayload(), other.isSetPayload());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPayload()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.payload, other.payload);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  @Override
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  @Override
  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  @Override
  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("PKEncryptedEnvelope(");
    boolean first = true;

    sb.append("pkEncryptedKey:");
    if (this.pkEncryptedKey == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.pkEncryptedKey, sb);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("keyEncryptionAlgorithm:");
    if (this.keyEncryptionAlgorithm == null) {
      sb.append("null");
    } else {
      sb.append(this.keyEncryptionAlgorithm);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("payloadEncryptionAlgorithm:");
    if (this.payloadEncryptionAlgorithm == null) {
      sb.append("null");
    } else {
      sb.append(this.payloadEncryptionAlgorithm);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("payload:");
    if (this.payload == null) {
      sb.append("null");
    } else {
      org.apache.thrift.TBaseHelper.toString(this.payload, sb);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (pkEncryptedKey == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'pkEncryptedKey' was not present! Struct: " + toString());
    }
    if (keyEncryptionAlgorithm == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'keyEncryptionAlgorithm' was not present! Struct: " + toString());
    }
    if (payloadEncryptionAlgorithm == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'payloadEncryptionAlgorithm' was not present! Struct: " + toString());
    }
    if (payload == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'payload' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class PKEncryptedEnvelopeStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public PKEncryptedEnvelopeStandardScheme getScheme() {
      return new PKEncryptedEnvelopeStandardScheme();
    }
  }

  private static class PKEncryptedEnvelopeStandardScheme extends org.apache.thrift.scheme.StandardScheme<PKEncryptedEnvelope> {

    @Override
    public void read(org.apache.thrift.protocol.TProtocol iprot, PKEncryptedEnvelope struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // PK_ENCRYPTED_KEY
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.pkEncryptedKey = iprot.readBinary();
              struct.setPkEncryptedKeyIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // KEY_ENCRYPTION_ALGORITHM
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.keyEncryptionAlgorithm = iprot.readString();
              struct.setKeyEncryptionAlgorithmIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // PAYLOAD_ENCRYPTION_ALGORITHM
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.payloadEncryptionAlgorithm = iprot.readString();
              struct.setPayloadEncryptionAlgorithmIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 20: // PAYLOAD
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.payload = iprot.readBinary();
              struct.setPayloadIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    @Override
    public void write(org.apache.thrift.protocol.TProtocol oprot, PKEncryptedEnvelope struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.pkEncryptedKey != null) {
        oprot.writeFieldBegin(PK_ENCRYPTED_KEY_FIELD_DESC);
        oprot.writeBinary(struct.pkEncryptedKey);
        oprot.writeFieldEnd();
      }
      if (struct.keyEncryptionAlgorithm != null) {
        oprot.writeFieldBegin(KEY_ENCRYPTION_ALGORITHM_FIELD_DESC);
        oprot.writeString(struct.keyEncryptionAlgorithm);
        oprot.writeFieldEnd();
      }
      if (struct.payloadEncryptionAlgorithm != null) {
        oprot.writeFieldBegin(PAYLOAD_ENCRYPTION_ALGORITHM_FIELD_DESC);
        oprot.writeString(struct.payloadEncryptionAlgorithm);
        oprot.writeFieldEnd();
      }
      if (struct.payload != null) {
        oprot.writeFieldBegin(PAYLOAD_FIELD_DESC);
        oprot.writeBinary(struct.payload);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class PKEncryptedEnvelopeTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    @Override
    public PKEncryptedEnvelopeTupleScheme getScheme() {
      return new PKEncryptedEnvelopeTupleScheme();
    }
  }

  private static class PKEncryptedEnvelopeTupleScheme extends org.apache.thrift.scheme.TupleScheme<PKEncryptedEnvelope> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, PKEncryptedEnvelope struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      oprot.writeBinary(struct.pkEncryptedKey);
      oprot.writeString(struct.keyEncryptionAlgorithm);
      oprot.writeString(struct.payloadEncryptionAlgorithm);
      oprot.writeBinary(struct.payload);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, PKEncryptedEnvelope struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      struct.pkEncryptedKey = iprot.readBinary();
      struct.setPkEncryptedKeyIsSet(true);
      struct.keyEncryptionAlgorithm = iprot.readString();
      struct.setKeyEncryptionAlgorithmIsSet(true);
      struct.payloadEncryptionAlgorithm = iprot.readString();
      struct.setPayloadEncryptionAlgorithmIsSet(true);
      struct.payload = iprot.readBinary();
      struct.setPayloadIsSet(true);
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

